**usage:**
```
 pak action package(s)
```

**action:** pak uses most of the pacman's flags to perform actions on packages (check 'man pacman' for more info). However there are few exceptions that combines pacman with other tools
```
 -Syu / -Sy / -Su/ update	- updates installed packages (sudo pacman -Syu)
							- checks for AUR updates (if 'cower' is installed)
							- installs AUR updates using 'git' and 'makepkg'
							- validates PKGBUILD and built package against errors
							  (if 'namcap' is installed)

 -Sc / clean				removes copies of uninstalled packages (sudo pacman -Sc)
							- and downloaded files of not installed packages from AUR cache

 -SS / search-all			searches for package(s) everywhere
							- in repositories and AUR
							- using 'pacman' and 'cower'

 -SI / info-all				prints information on given package(s) from everywhere
							- from repositories and AUR
							- using 'pacman' and 'cower'
```


**action - AUR part:**  list of flags used to manage AUR packages
```
 -SA / install-aur			installs new package(s) from AUR
							- using 'git clone/pull' and 'makepkg' afterwards
							- validates PKGBUILD and built package against errors
							  (if 'namcap' is installed)

 -SsA / search-aur			searches for package(s) in AUR
							- using 'cower'

 -SiA / info-aur			prints information on given AUR package(s)
							- using 'cower'

 -ScA / clean-aur			removes downloaded files of not installed packages from AUR cache
```


**action - download part:**  pak is able to download PKGBUILDs of official packages (asp) and AUR PKGBUILDs (git)
```
 -G / download          downloads PKGBUILDs of packages from repositories
                        - using 'asp export'
                        - with the ability to choose what repository to download from

 -GA / download-aur     downloads PKGBUILDs of AUR packages
                        - using 'git clone/pull'
```

**examples:**
```
 pak -S foo             install 'foo' from repositories (sudo pacman -Syu foo)
 pak -SA bar            install 'bar' from AUR
 pak -SsA foo editor    search 'foo editor' in AUR (cower -s foo editor)
 pak -SiA foo bar		show info about 'foo' and 'bar' from AUR (cower -i foo bar)
 pak -GA foo bar        downloads PKGBUILDs of 'foo' and 'bar' from AUR
```

**Default configuration file:** (/etc/xdg/pak.conf) can be overwritten by copying above file to $HOME/.config/pak/pak.conf
