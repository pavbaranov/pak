pak                                                                                                 0000700 0001750 0001731 00000030112 13331566172 010616  0                                                                                                    ustar   nycko                           users                                                                                                                                                                                                                  #!/bin/bash

# colors
RESET="\e[0m"
RED="\e[1;31m"
GREEN="\e[1;32m"
YELLOW="\e[1;33m"
BLUE="\e[1;34m"
MAGENTA="\e[1;35m"
CYAN="\e[1;36m"
BOLD="\e[1;39m"

# localization
export TEXTDOMAIN='pak'
export TEXTDOMAINDIR='/usr/share/locale'

# path to configuration file
xdg_config="${XDG_CONFIG_HOME:-${HOME}/.config}"

if [[ -e "${xdg_config}/pak/pak.conf" ]]; then
	source ${xdg_config}/pak/pak.conf
	echo -e "$(gettext 'User configuration file is used:') ${xdg_config}/pak/pak.conf\n"
elif [[ -e "/etc/xdg/pak.conf" ]]; then
	source /etc/xdg/pak.conf
else
	echo -e "${RED}$(gettext 'ERROR:')${RESET} $(gettext 'No configuration file found! Check if /etc/xdg/pak.conf exists.')"
	exit
fi

### functions ###
usage() {
	echo -e "${BLUE}::${BOLD} $(gettext 'usage:')\n pak ${GREEN}$(gettext 'action')${RESET} $(gettext 'package(s)')"
	echo
	echo -e "${BLUE}::${BOLD} ${GREEN}$(gettext 'action')${RESET}"
	echo -e " $(gettext "pak uses all the pacman's flags to perform actions on packages")"
	echo -e " $(gettext "check 'man pacman' for more info")"
	echo -e " $(gettext "However there are couple aliases to tasks that combines pacman with other tools")"
	echo
	echo -e " ${GREEN}-Syu${RESET} / ${GREEN}update${RESET} / ${GREEN}-U${RESET}\t$(gettext "updates installed packages") (sudo pacman -Syu)"
	echo -e "\t\t\t- $(gettext "checks for AUR updates ONLY IF 'cower' is installed")"
	echo -e "\t\t\t- $(gettext "installs AUR updates using 'git' and 'makepkg'")\n"
	echo -e " ${GREEN}-Sc${RESET} / ${GREEN}clean${RESET} / ${GREEN}-C${RESET}\t$(gettext "removes copies of uninstalled packages") (sudo pacman -Sc)"
	echo -e "\t\t\t- $(gettext "from both pacman and AUR cache")\n"
	echo	
	echo -e "${BLUE}::${BOLD} ${GREEN}$(gettext 'action')${RESET} - $(gettext 'AUR part:')"
	echo -e " $(gettext 'list of flags used to manage AUR packages')"
	echo
	echo -e " ${GREEN}-SA${RESET} / ${GREEN}install-aur${RESET}\t$(gettext 'installs new package(s) from AUR')"
	echo -e "\t\t\t- $( gettext "using 'git clone/pull' and 'makepkg' afterwards")\n"
	echo -e " ${GREEN}-SsA${RESET} / ${GREEN}search-aur${RESET}\t$(gettext "searches for package(s) in AUR")"
	echo -e "\t\t\t- $(gettext "using 'cower'")\n"
	echo -e " ${GREEN}-SiA${RESET} / ${GREEN}info-aur${RESET}\t$(gettext "prints information on given AUR package(s)")"
	echo -e "\t\t\t- $(gettext "using 'cower'")\n"
	echo
	echo -e "${BLUE}::${BOLD} ${GREEN}$(gettext 'action')${RESET} - $(gettext 'download part:')"
	echo -e " $(gettext "pak is able to download PKGBUILDs of official packages (asp) and AUR PKGBUILDs (git)")"
	echo
	echo -e " ${GREEN}-G${RESET} / ${GREEN}download${RESET}\t\t$(gettext "downloads PKGBUILDs of packages from repositories")"
	echo -e "\t\t\t- $(gettext "using 'asp export'")"
	echo -e "\t\t\t- $(gettext "with the ability to choose what repository to download from")"
	echo -e " ${GREEN}-GA${RESET} / ${GREEN}download-aur${RESET}\t$(gettext "downloads PKGBUILDs of AUR packages")"
	echo -e "\t\t\t- $(gettext "using 'git clone/pull'")"
	echo
	echo -e "${BLUE}:: ${BOLD}$(gettext 'examples:')${RESET}"
	echo -e " pak -S foo\t\t$(gettext "install 'foo' from repositories")"
	echo -e " pak -SA bar\t\t$(gettext "install 'bar' from AUR")"
	echo -e " pak -SsA foo editor\t$(gettext "search 'foo editor' in AUR")"
	echo -e " pak -GA foo bar\t$(gettext "downloads PKGBUILDs of 'foo' and 'bar' from AUR")"
	echo
	echo -e "${BLUE}:: ${BOLD}$(gettext 'Default configuration file:')${RESET}"
	echo -e " ${GREEN}/etc/xdg/pak.conf${RESET}"
	echo -e " $(gettext 'can be overwritten by copying above file to') $HOME/.config/pak/pak.conf"
#	echo "#######################################################################"

}

build() {
	echo -e "${BLUE}::${RESET} ${BOLD}$(gettext 'Edit PKGBUILD? [Y/n]')${RESET}" && read -r k
	if [[ "$k" != "n" ]]; then
		${EDITOR:-nano} PKGBUILD
	fi
	makepkg "$makepkg_flags"
	sudo cp -fv *.pkg.tar.xz "${pkg_dest}"
	\rm -f *.tar* *.deb *.rpm *.gem *.asc && echo
}

install_aur() {
	if [[ -e "$git_path" ]]; then
		i=1; while [ "$i" -le "$#"  ]; do
			eval "arg=\${$i}"

			if [[ -d "${pak_cache}/${arg}" ]]; then 
				cd "${pak_cache}/${arg}"
				git fetch --all > /dev/null
				git reset --hard > /dev/null
				git pull "${aur_link}/${arg}"
				build
			else
				cd "${pak_cache}"
				git clone "${aur_link}/${arg}"
				cd "${pak_cache}/$arg"
				build
			fi
			(( i++ ))
		done
	else
		echo "$(gettext 'git is not installed.')"
	fi
}

update() {
	sudo pacman -Syu

	# aur
	echo -e "\n${BLUE}::${RESET} ${BOLD}$(gettext 'Starting AUR update')${RESET}..."
	if [[ -e "${cower_path}" ]] || [[ -e "$git_path" ]]; then
		cower -u > "${pak_updatelist}"

		# print available updates to screen and packages names to array
		if [[ ! -s "${pak_updatelist}" ]]; then
			echo -e  " $(gettext 'No updates found:') $(gettext 'nothing to do')"
		else
			echo -e "${BOLD}$(gettext 'AUR packages')\t\t$(gettext 'Local version')\t\t$(gettext 'AUR version')${RESET}\n"
		
			#nameArr=()
			while IFS= read -r line || [[ "$line" ]]; do
				local pkg_name=$( echo "$line" | awk '{print $2}' )
				echo -n "${pkg_name} " >> /tmp/pak-toupdate
				local pkg_oldver=$( echo "$line" | awk '{print $3}' )
				local pkg_newver=$( echo "$line" | awk '{print $5}' )
				
				echo -e "${pkg_name}\t\t${RED}${pkg_oldver}${RESET}\t\t\t${GREEN}${pkg_newver}${RESET}"
				#nameArr+=( "$pkg_name" )
			done < "${pak_updatelist}"

			# updating AUR packages
			echo -e "${BLUE}::${RESET} ${BOLD}$(gettext 'Update packages? [Y/n]')${RESET}" && read -r k
			if [[ "$k" = "n" ]]; then
				echo "$(gettext 'Aborted by user.')"
			else
				#for i in ${nameArr[@]}; do
				#	if [[ -d "${pak_cache}/${nameArr[$i]}" ]]; then
				#		cd "${pak_cache}/${nameArr[$i]}"
				#		git fetch --all > /dev/null
				#		git reset --hard > /dev/null
				#		git pull "${aur_link}/${nameArr[$i]}"
				#		build
				#	else
				#		cd "${pak_cache}"
				#		git clone "${aur_link}/${nameArr[$i]}"
				#		cd "${pak_cache}/${nameArr[$i]}"
				#		build
				#	fi
				#done
				#cat /tmp/pak-toupdate
				install_aur $(cat /tmp/pak-toupdate)
				rm /tmp/pak-toupdate
			fi
		fi
		
		# AUR warnings: orphan, out of date, versions mismatch
		if [[ ${aur_warn} = 1 ]]; then
			cower -i --format "%n %m %t %v\n" $(pacman -Qqm | grep -vE "git|svn") > "${pak_aurwarnings}" && echo

			n=1; while IFS= read -r line || [[ "$line"   ]]; do
				local pkg=$( echo $line | awk '{printf $1}' )
				local maintainer=$( echo $line | awk '{printf $2}' )
				local flag=$( echo $line | awk '{printf $3}' )
				local version=$( echo $line | awk '{printf $4}' )
				local local_version=$( pacman -Q $pkg | awk '{printf $2}' )

				[[ $maintainer == "(orphan)"  ]] && maintainer="${RED}$maintainer${RESET}"
				[[ $flag == "yes"  ]] && flag="${RED}$flag${RESET}"
				[[ $version != $local_version ]] && local_version="${RED}$local_version${RESET}"
				# print packages
				[[ $( echo "$maintainer" | grep -F "$RED"  ) || $( echo "$flag" | grep -F "$RED"  )  ]] || [[ $( echo "$local_version" | grep -F "$RED" ) ]] && echo -e "${YELLOW}$(gettext 'warning:')${RESET} $pkg: $(gettext 'installed:') $local_version, $(gettext 'available:') $version, $(gettext 'maintainer:') $maintainer, $(gettext 'flagged:') $flag"
				((n++))
			done < "${pak_aurwarnings}"
			
			# List of VCS packages
			if [[ "${vcs_list}" = 1 ]]; then
				echo -e "\n${BLUE}:: ${BOLD}$(gettext 'List of installed VCS packages:')${RESET}"
				pacman -Qm | grep -E "git|svn" | awk '{print $1 " \t\t  " $2}'
			fi
		fi
	else
		echo "$(gettext 'cower (AUR) is not installed.')"
	fi
}


clean() {
	# pacman
	sudo pacman -Sc --color=auto

	# aur
	find "${pak_cache}" -maxdepth 1 -type d -exec basename {} \; | tail -n +2 > "${pak_cachelist}"
	pacman -Qm > "${pak_locallist}"

	if [[ ! -s "${pak_cachelist}" ]]; then
		echo -e "\n${BLUE}::${RESET} AUR cache: $(gettext 'nothing to do')"
	else
		echo -e "\n$(gettext 'AUR cache directory:') ${pak_cache}"
		echo -e "${BLUE}::${RESET} ${BOLD}$(gettext 'Do you want to remove all not installed packages from AUR cache? [Y/n]')${RESET}" && read -r k
		if [[ "$k" = "n" ]]; then
			echo "$(gettext 'Aborted by user.')"
		else
			#echo -e "$(gettext 'removing old packages from cache...')\n"
			i=1; while IFS= read -r line || [[ "$line" ]]; do
				local test=$(cat "${pak_locallist}" | grep "$line")
				if [[ -n "$test" ]]; then
					continue
				else
					\rm -rf "${pak_cache}/$line"
					echo "$line $(gettext 'has been removed')"
				fi
				((i++))
				done < "${pak_cachelist}"
		fi
		echo "$(gettext 'Done')"
	fi
}

search_aur() {
	if [[ -e "${cower_path}" ]]; then
		cower -s --color=auto "$@" 
	else
		echo "$(gettext 'cower (AUR) is not installed.')"
	fi
}

info_aur() {
	if [[ -e "${cower_path}" ]]; then
		cower -i --color=auto "$@" 
	else
		echo "$(gettext 'cower (AUR) is not installed.')"
	fi
}

download() {
	aur() {
		if [[ -e "$git_path" ]]; then
			i=1
			while [ "$i" -le "$#"  ]; do
				eval "arg=\${$i}"

				if [[ -d "${download_path}/${arg}" ]]; then 
					cd "${download_path}/${arg}"
					git fetch --all > /dev/null
					git reset --hard > /dev/null
					git pull "${aur_link}/${arg}" && echo
				else
					cd "${download_path}"
					git clone "${aur_link}/${arg}"
					cd "${download_path}/${arg}" && echo
				fi
				((i++))
			done
			echo "$(gettext 'PKGBUILDs have been downloaded to') ${download_path}"
		else
			echo "$(gettext 'git is not installed.')"
		fi
	}

	polaur() {
		#echo "$@"
		echo "Not implemented yet"
	}

	repo() {
		i=1
		if [ -e "$asp_path" ]; then
			while [ "$i" -le "$#"  ]; do
				eval "arg=\${$i}"

				cd "${download_path}"
				
				# writing repositories with given package to file
				pacman -Si "$arg" | grep "Repo" | awk {'print $3'} > /tmp/repos

				[[ ! -s /tmp/repos ]] && break

				# putting repositories to array
				repoArr=()
				n=1; while IFS= read -r line || [[ "$line" ]]; do
					local repo="$( echo "$line" )"
					repoArr+=( "$repo" )
				done < /tmp/repos
				
				# printing menu to choose download source
				echo -e "${BLUE}:: ${GREEN}$arg${RESET}-> $(gettext 'Choose repository to download from (default=0):')"
				m=0; for j in ${repoArr[@]}; do
					#echo ${repoArr[$m]}
					echo -e "${YELLOW}$m${RESET}. ${repoArr[$m]}"
					((m++))
				done

				read -r k
				num=$( echo "$k" | grep -E "^\-?[0-9]+$" )

				[[ ! "$k" ]] && k=0
				
				# downloading chosen PKGBUILDs
				if [[ "$num" -ge 0 && "$num" -lt "$m" ]]; then
					# fix asp's weird behavior when downloading from "stable" repositories
					if [[ ${repoArr[$num]} = "core" || ${repoArr[$num]} = "community" || ${repoArr[$num]} = "extra" ]]; then
						"$asp_path" -f export "$arg" && echo
					else
						"$asp_path" -f export "${repoArr[$num]}/${arg}" && echo
					fi
				else
					echo -e "$(gettext "You've chosen the wrong repository.")\n"
				fi

				((i++))
			done
			echo "$(gettext 'PKGBUILDs have been downloaded to') ${download_path}"
		else
			echo "$(gettext 'asp is not installed.')"
		fi
	}
	
	case "$1" in
		-G | download)
			repo $( echo "$@" | cut -d ' ' -f2- ) ;;
		-GA | -Ga | download-aur)
			aur $( echo "$@" | cut -d ' ' -f2- ) ;;
		--polaur)
			polaur $( echo "$@" | cut -d ' ' -f2- ) ;;
	esac
}

### program ###
if [[ $(id -u) = 0 ]]; then
	echo -e "${RED}$(gettext "This program doesn't run from a privileged user account.")${RESET}"
	echo -e "$(gettext "Run it as normal user (without sudo)")"
	exit
else
	# create tmp files
	[[ ! -d "$pak_cache" ]] && mkdir -p "$pak_cache"
	[[ ! -d "$download_path" ]] && mkdir -p "$download_path"
	[[ ! -e "$pak_locallist" ]] && touch "$pak_locallist"
	[[ ! -e "$pak_cachelist" ]] && touch "$pak_cachelist"
	[[ ! -e "$pak_updatelist" ]] && touch "$pak_updatelist"
	[[ ! -e "$pak_aurwarnings" ]] && touch "$pak_aurwarnings"

	action=${1:-"-h"}
	case $action in
		-Syu | -Sy | -Su | update | -U)
			update ;;
		-SA | -Sa | install-aur)
			install_aur $( echo "$@" | cut -d ' ' -f2- ) ;;
		-SsA | -Ssa | search-aur)
			search_aur $( echo "$@" | cut -d ' ' -f2- ) ;;
		-SiA | -Sia | info-aur)
			info_aur $( echo "$@" | cut -d ' ' -f2- ) ;;
		-G | -GA | -Ga | download | download-aur)
			download "$@" ;;
		-Sc | clean | -C)
			clean ;;
		-h | --help)
			usage ;;
		-S | -Sd | -Fy | -D* | -R*)
			sudo pacman "$@" ;;
		*)
			pacman "$@" ;;
	esac
	# remove tmp files
	\rm  "${pak_cachelist}" "${pak_locallist}" "${pak_updatelist}" "${pak_aurwarnings}"
fi
                                                                                                                                                                                                                                                                                                                                                                                                                                                      pak.conf                                                                                            0000600 0001750 0001731 00000002067 13330271011 011531  0                                                                                                    ustar   nycko                           users                                                                                                                                                                                                                  ### pak configuration ###

# user's default cache path: $XDG_CACHE_HOME or $HOME/.cache
xdg_cache="${XDG_CACHE_HOME:-${HOME}/.cache}"

# program's main cache
pak_cache="${xdg_cache}/pak"

# path to downloaded PKGBUILDs (-G / -GA options)
download_path="${xdg_cache}/pak-downloaded"

# destination path of built packages (default: /var/cache/pacman/pkg)
pkg_dest="/var/cache/pacman/pkg"

# show AUR warnings: 0/1 (orphan, out of date, version mismatch)
aur_warn=1
# show installed VCS packages: 0/1 (-git, -svn)
vcs_list=1

# makepkg (man makepkg) - it's used only in install part of this script, -si flag is a MUST
makepkg_flags="-fsirc"

# path to cower binary (default: /usr/bin/cower)
cower_path="/usr/bin/cower"
# path to git binary (default: /usr/bin/git)
git_path="/usr/bin/git"
# path to asp binary (default: /usr/bin/asp)
asp_path="/usr/bin/asp"

# AUR protocols (https://, http://)
aur_link="https://aur.archlinux.org"

# temporary files paths
pak_locallist="/tmp/pak_local"
pak_cachelist="/tmp/pak_cache"
pak_updatelist="/tmp/pak_update"
pak_aurwarnings="/tmp/pak_aur"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                         locale/pl.mo                                                                                        0000600 0001750 0001731 00000013140 13330271515 012321  0                                                                                                    ustar   nycko                           users                                                                                                                                                                                                                  ��    @        Y         �     �     �  	   �     �     �  /   �     �  F        ]     b     i  O   ~     �     �  ?   �     <  !   N  $   p     �  8   �     �      �  #        >     E  
   [  +   f      �  3   �     �     	  /   	  #   D	  1   h	  	   �	     �	     �	     �	     �	     �	     

  
   *
  .   5
      d
  )   �
     �
     �
  
   �
  T   �
  >   )  *   h  &   �  #   �     �     �          3     :     M     [  /   r     �  ;   �  �   �  !   �     	          (     5  8   S     �  N   �     �            ^   &  $   �     �  S   �          )  +   G     s  @   �     �      �  !        4     =  
   X  5   c  3   �  C   �  "     $   4  %   Y       (   �     �     �     �     �          "     8     X  9   g     �  +   �     �     �       V     I   l  +   �  "   �  8        >     W      j     �     �     �     �  3   �       B        0   /   ?                "                             	   *                     >      &   4      8      $   @            +   (            6             '      )         =              #          %   7   .   ,       :           
       1                         <   3      ;       9          !   5      -           2        AUR cache directory: AUR packages AUR part: AUR version Aborted by user. Choose repository to download from (default=0): Default configuration file: Do you want to remove all not installed packages from AUR cache? [Y/n] Done ERROR: Edit PKGBUILD? [Y/n] However there are couple aliases to tasks that combines pacman with other tools List of installed VCS packages: Local version No configuration file found! Check if /etc/xdg/pak.conf exists. No updates found: PKGBUILDs have been downloaded to Run it as normal user (without sudo) Starting AUR update This program doesn't run from a privileged user account! Update packages? [Y/n] User configuration file is used: You've chosen the wrong repository. action asp is not installed. available: can be overwritten by copying above file to check 'man pacman' for more info checks for AUR updates ONLY IF 'cower' is installed cower (AUR) is not installed. download part: downloads PKGBUILDs of 'foo' and 'bar' from AUR downloads PKGBUILDs of AUR packages downloads PKGBUILDs of packages from repositories examples: flagged: from both pacman and AUR cache git is not installed. has been removed install 'bar' from AUR install 'foo' from repositories installed: installs AUR updates using 'git' and 'makepkg' installs new package(s) from AUR list of flags used to manage AUR packages maintainer: nothing to do package(s) pak is able to download PKGBUILDs of official packages (asp) and AUR PKGBUILDs (git) pak uses all the pacman's flags to perform actions on packages prints information on given AUR package(s) removes copies of uninstalled packages removing old packages from cache... search 'foo editor' in AUR searches for package(s) in AUR updates installed packages usage: using 'asp export' using 'cower' using 'git clone/pull' using 'git clone/pull' and 'makepkg' afterwards warning: with the ability to choose what repository to download from Project-Id-Version: 1.3
POT-Creation-Date: 2018-07-31 12:35+0100
PO-Revision-Date: 2018-07-31 12:35+0100
Last-Translator: Damian N. <nycko123 at gmail>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Katalog pamięci podręcznej AUR: Paczki z AUR część dot. AUR Wersja w AUR Przerwany przez użytkownika. Wybierz, z którego repozytorium pobrać (domyślnie=0): Domyślny plik z konfiguracją: Czy chcesz usunąć niezainstalowane pakiety z pamięci podręcznej AUR? [T/n] Wykonano BŁĄD: Edytować PKGBUILD? [T/n] Istnieją jednak dwa dodatkowe polecenia dla zadań łączących pacmana z innymi narzędziami Lista zainstalowanych pakietów VCS: Zainstalowana Nie znaleziono pliku z konfiguracją! Sprawdź czy plik /etc/xdg/pak.conf istnieje. Nie znaleziono aktualizacji: PKGBUILDy zostały pobrane do Uruchom jako zwykły użytkownik (bez sudo) Uruchamianie aktualizacji z AUR Ten program nie działa z kontem uprzywilejowanego użytkownika! Zaktualizować paczki? [T/n] Używany jest plik użytkownika: Wybrano niepoprawne repozytorium. operacja asp nie jest zainstalowany dostępna: może być nadpisany przez skopiowanie powyższego do sprawdź 'man pacman', by dowiedzieć się więcej sprawdza aktualizacje w AUR TYLKO JEŚLI 'cower' jest zainstalowany cower (AUR) nie jest zainstalowany część dot. pobierania PKGBUILDów pobiera PKGBUILDy 'foo' i 'bar' z AUR pobiera PKGBUILDy paczek z AUR pobiera PKGBUILDy paczek z repozytoriów przykłady: oflagowana: z cache pacmana i AUR git nie jest zainstalowany został usunięty instaluje 'bar' z AUR instaluje 'foo' z repozytoriów zainstalowana: instaluje aktualizacje z AUR używając 'git' i 'makepkg' instaluje nową paczkę z AUR lista operacji dostępnych do użycia z AUR opiekun: nie ma nic do zrobienia paczka/i pak potrafi pobrać PKGBUILDy oficjalnych paczek (asp) oraz PKGBUILDy paczek AUR (git) pak korzysta z wszystkich flag pacmana, aby wykonać operacje na paczkach wypisuje informacje o podanych paczkach AUR usuwa kopie odinstalowanych paczek usuwanie starych pakietów z pamięci podręcznej AUR... szuka 'foo editor' w AUR szuka paczek w AUR aktualizuje zainstalowane paczki użycie: używając 'asp export' używając 'cower' używając 'git clone/pull' używając 'git clone/pull', a następnie 'makepkg' ostrzeżenie: z możliwością wyboru, z którego repozytorium dokonać pobrania                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 